declare module "@braintree/sanitize-url" {
  export function sanitizeUrl(url: string): string;
}

declare module "canvg";
declare module "instascan";
