import { storiesOf, moduleMetadata } from "@storybook/angular";
import { withKnobs, number } from "@storybook/addon-knobs";
import { RouterTestingModule } from "@angular/router/testing";
import { ScrollingModule } from "@angular/cdk/scrolling";
import { HttpClientModule } from "@angular/common/http";
import { InlineSVGModule } from "ng-inline-svg";

import { PopupComponent, PopupItemComponent } from "~/app/extension/popup";
import { SharedModule } from "~/app/shared";

storiesOf("Extension", module)
  .addDecorator(withKnobs)
  .addDecorator(
    moduleMetadata({
      imports: [
        RouterTestingModule,
        ScrollingModule,
        SharedModule,
        InlineSVGModule.forRoot(),
        HttpClientModule
      ],
      declarations: [PopupComponent, PopupItemComponent]
    })
  )
  .add("Popup", () => ({
    component: PopupComponent,
    props: {
      secrets: [
        {
          name: "Google",
          id: 1,
          type: "website",
          data: {
            url: "www.google.com",
            username: "gman"
          },
          secret_through_set: [
            {
              data: {}
            }
          ]
        }
      ],
      matchedSecrets: [],
      search: "",
      selectedSecret: number("selectedSecret", 2),
      usernameCopied: true,
      passwordCopied: true
    }
  }));
