import { storiesOf, moduleMetadata } from "@storybook/angular";
import { withKnobs } from "@storybook/addon-knobs";
import { RouterTestingModule } from "@angular/router/testing";
import { SharedModule } from "~/app/shared";

storiesOf("Shared", module)
  .addDecorator(withKnobs)
  .addDecorator(
    moduleMetadata({
      imports: [
        RouterTestingModule,
        SharedModule
      ]
    })
  )
  .add("Aside Link", () => ({
    template: `
      <aside-link>
        Here's an aside <em>link</em>!
      </aside-link>
    `
  }));
