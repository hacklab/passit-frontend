import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupPendingListComponent } from './group-pending-list.component';

describe('GroupPendingListComponent', () => {
  let component: GroupPendingListComponent;
  let fixture: ComponentFixture<GroupPendingListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupPendingListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupPendingListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
