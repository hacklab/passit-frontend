import { IContact } from "../group/contacts/contacts.interfaces";
import { ISelectOptions } from "./group-form/group-form.interfaces";
// tslint:disable:max-line-length
export const testContacts: IContact[] = [
  {
    id: 1,
    email: "test@gmail.com",
    first_name: "",
    last_name: ""
  },
  {
    id: 2,
    email: "test2@gmail.com",
    first_name: "",
    last_name: ""
  }
];

export const testContact: ISelectOptions = {
  label: "test@gmail.com",
  value: 1,
  disabled: false
};
