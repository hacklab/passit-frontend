import { Component, Input, EventEmitter, Output } from "@angular/core";
import { BackupCodePdfService } from "../backup-code-pdf.service";

@Component({
  selector: "app-backup-code",
  templateUrl: "./backup-code.component.html",
  styleUrls: ["../account.component.scss"]
})
export class BackupCodeComponent {
  @Input()
  code: string;

  @Output()
  goToNext = new EventEmitter();

  constructor(private backupCodeToPdf: BackupCodePdfService) {
  }

  download() {
    this.backupCodeToPdf.download(this.code);
  }
}
