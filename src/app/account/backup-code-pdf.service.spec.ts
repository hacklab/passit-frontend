import { TestBed, inject } from '@angular/core/testing';

import { BackupCodePdfService } from './backup-code-pdf.service';

describe('BackupCodePdfService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BackupCodePdfService]
    });
  });

  it('should be created', inject([BackupCodePdfService], (service: BackupCodePdfService) => {
    expect(service).toBeTruthy();
  }));
});
