import { Component } from "@angular/core";
import { Store, select } from "@ngrx/store";
import { selectChangePasswordNewBackupCode } from "../account.reducer";
import { ChangePasswordSubmitForm, ResetForm } from "./change-password.actions";
import * as fromRoot from "../../app.reducers";
import * as fromAccount from "../account.reducer";
import * as fromChangePassword from "./change-password.reducer";
import { LogoutAction } from "../account.actions";
import { SetValueAction } from "ngrx-forms";

@Component({
  selector: "change-password-container",
  template: `
    <change-password
      [form]="form$ | async"
      [hasStarted]="hasStarted$ | async"
      [hasFinished]="hasFinished$ | async"
      [nonFieldErrors]="errorMessage$ | async"
      [backupCode]="backupCode"
      (changePassword)="changePassword()"
      (toggleConfirm)="toggleConfirm()"
      (reset)="reset()"
      (onForgotPassword)="onForgotPassword()"
    >
    </change-password>
  `
})
export class ChangePasswordContainer {
  form$ = this.store.pipe(select(fromAccount.changePassword));
  hasStarted$ = this.store.pipe(select(fromAccount.changePasswordHasStarted));
  hasFinished$ = this.store.pipe(select(fromAccount.changePasswordHasFinished));
  errorMessage$ = this.store.select(fromAccount.changePasswordErrorMessage);
  backupCode: string;
  showConfirm: boolean;
  nonFieldErrors: string[];
  clickMessage = "";

  constructor(private store: Store<fromRoot.IState>) {
    this.store
      .select(selectChangePasswordNewBackupCode)
      .subscribe(backupCode =>
        backupCode ? (this.backupCode = backupCode) : null
      );
    this.form$.subscribe(
      form => (this.showConfirm = form.controls.showConfirm.value)
    );
  }

  toggleConfirm() {
    this.store.dispatch(
      new SetValueAction(
        fromChangePassword.FORM_ID + ".showConfirm",
        !this.showConfirm
      )
    );
  }

  onForgotPassword() {
    if (
      window.confirm(
        "Account recovery will log you out. Have your backup code ready, and download your passwords just in case."
      )
    ) {
      this.store.dispatch(new LogoutAction());
    }
  }

  reset() {
    this.store.dispatch(new ResetForm());
  }

  changePassword() {
    this.store.dispatch(new ChangePasswordSubmitForm());
  }
}
