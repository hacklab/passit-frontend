import { Component } from "@angular/core";
import { Store } from "@ngrx/store";
import { IState } from "../../../app.reducers";
import { LogoutAction } from "../.././account.actions";

@Component({
  selector: "forgot-learn-more-container",
  template: `
    <forgot-learn-more
      [confirmText]="confirmText"
      (logOut)="logOut()"
    ></forgot-learn-more>
  `
})
export class ForgotLearnMoreContainer {
  confirmText =
    "Please confirm that you have your backup code ready before logging out.";

  constructor(private store: Store<IState>) {}

  logOut() {
    this.store.dispatch(new LogoutAction());
  }
}
