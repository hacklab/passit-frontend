// tslint:disable:max-line-length
import { Component, ChangeDetectionStrategy, Input } from "@angular/core";
import { AbstractControlState, ValidationErrors } from "ngrx-forms";

@Component({
  selector: "app-server-select",
  template: `
    <app-checkbox
      title="My company has its own Passit server"
      subtext="If you signed up for Passit anywhere other than app.passit.io (e.g. passit.mycompany.com, or your self-hosted server), you’ll need to specify where you want to log&nbsp;in."
      [control]="showUrlControl"
      tabindex="5"
    ></app-checkbox>

    <div *ngIf="showUrlControl.value" class="form-field form-field--large">
      <label [for]="urlControl.id" class="form-field__label">Server URL</label>
      <input
        type="url"
        [ngrxFormControlState]="urlControl"
        class="form-field__input"
        [class.form-field__input--invalid]="formErrors._url"
        [class.form-field__input--one-action]="urlControl.isValidationPending"
        tabindex="6"
      >
      <div class="form-field__actions" *ngIf="urlControl.isValidationPending">
        <progress-indicator
          [inProgress]="urlControl.isValidationPending"
          [inputAction]="true"></progress-indicator>
      </div>
    </div>
    <ul class="form-field__error-list">
      <li *ngIf="formErrors._url?.$exists" class="form-field__error">
        Cannot connect to {{ formErrors._url?.$exists }}
      </li>
    </ul>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ServerSelectComponent {
  @Input()
  showUrlControl: AbstractControlState<boolean>;
  @Input()
  urlControl: AbstractControlState<string>;
  @Input()
  formErrors: ValidationErrors;

  constructor() {}
}
