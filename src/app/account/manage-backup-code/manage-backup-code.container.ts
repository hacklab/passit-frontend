import { Component, ChangeDetectionStrategy } from "@angular/core";
import * as fromAccount from "../account.reducer";
import { Store, select } from "@ngrx/store";
import { SubmitForm, ResetForm } from "./manage-backup-code.actions";
import { selectManageBackupCodeNewBackupCode } from "../account.reducer";
import { BackupCodePdfService } from "../backup-code-pdf.service";
import { SetValueAction } from "ngrx-forms";
import { FORM_ID } from "./manage-backup-code.reducer";

@Component({
  template: `
    <app-manage-backup-code
      [form]="form$ | async"
      [hasStarted]="hasStarted$ | async"
      [hasFinished]="hasFinished$ | async"
      [nonFieldErrors]="errorMessage$ | async"
      [backupCode]="backupCode"
      (newBackupCode)="newBackupCode()"
      (getBackupPDF)="getBackupPDF()"
      (reset)="reset()"
      (toggleShowConfirm)="toggleConfirm()"
    ></app-manage-backup-code>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ManageBackupCodeContainer {
  form$ = this.store.pipe(select(fromAccount.manageBackupCode));
  hasStarted$ = this.store.pipe(select(fromAccount.manageBackupCodeHasStarted));
  hasFinished$ = this.store.pipe(
    select(fromAccount.manageBackupCodeHasFinished)
  );
  errorMessage$ = this.store.select(fromAccount.manageBackupCodeErrorMessage);
  backupCode: string;
  showConfirm = true;

  constructor(
    private store: Store<any>,
    private backupCodeToPdf: BackupCodePdfService
  ) {
    this.store
      .select(selectManageBackupCodeNewBackupCode)
      .subscribe(
        backupCode => (backupCode ? (this.backupCode = backupCode) : null)
      );
    this.form$.subscribe(
      form => (this.showConfirm = form.controls.showConfirm.value)
    );
  }

  getBackupPDF() {
    this.backupCodeToPdf.download(this.backupCode);
  }
  newBackupCode() {
    this.store.dispatch(new SubmitForm());
  }
  reset() {
    this.store.dispatch(new ResetForm());
  }
  toggleConfirm() {
    this.store.dispatch(
      new SetValueAction(FORM_ID + ".showConfirm", !this.showConfirm)
    );
  }
}
