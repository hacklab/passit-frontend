import {
  FormGroupState,
  updateGroup,
  validate,
  createFormGroupState,
  createFormStateReducerWithUpdate
} from "ngrx-forms";
import { IBaseFormState } from "~/app/utils/interfaces";
import { oldPasswordValidators } from "../constants";
import {
  ManageBackupCodeActionTypes,
  ManageBackupCodeActionsUnion
} from "./manage-backup-code.actions";
export const FORM_ID = "Manage Backup Code Form";

export interface IForm {
  oldPassword: string;
  showConfirm: boolean;
}

export interface IState extends IBaseFormState {
  form: FormGroupState<IForm>;
  errorMessage: string[] | null;
  newBackupCode: string | null;
}

const validateAndUpdateFormState = updateGroup<IForm>({
  oldPassword: validate(oldPasswordValidators)
});

export const newBackupCode = (state: IState) => state.newBackupCode;

export const initialFormState = validateAndUpdateFormState(
  createFormGroupState<IForm>(FORM_ID, {
    oldPassword: "",
    showConfirm: true
  })
);

const initialState: IState = {
  form: initialFormState,
  hasFinished: false,
  hasStarted: false,
  newBackupCode: null,
  errorMessage: null
};

export const formReducer = createFormStateReducerWithUpdate<IForm>(
  validateAndUpdateFormState
);

export function reducer(
  state = initialState,
  action: ManageBackupCodeActionsUnion
): IState {
  const form = formReducer(state.form, action);
  state = { ...state, form };

  switch (action.type) {
    case ManageBackupCodeActionTypes.SUBMIT_FORM:
      return {
        ...state,
        hasStarted: true,
        hasFinished: false,
        errorMessage: null,
        newBackupCode: null
      };

    case ManageBackupCodeActionTypes.SUBMIT_FORM_SUCCESS:
      return {
        ...state,
        hasFinished: true,
        newBackupCode: action.payload,
        errorMessage: null,
        hasStarted: false
      };

    case ManageBackupCodeActionTypes.SUBMIT_FORM_FAILURE:
      return {
        ...state,
        errorMessage: action.payload,
        hasFinished: false,
        hasStarted: false,
        newBackupCode: null
      };

    case ManageBackupCodeActionTypes.RESET_FORM:
      return initialState;
  }

  return state;
}

export const getForm = (state: IState) => state.form;
export const getErrorMessage = (state: IState) => state.errorMessage;
export const getHasStarted = (state: IState) => state.hasStarted;
export const getHasFinished = (state: IState) => state.hasFinished;
export const getNewBackupCode = (state: IState) => state.newBackupCode;
