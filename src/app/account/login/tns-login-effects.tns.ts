import { Injectable } from "@angular/core";
import { Effect, Actions, ofType } from "@ngrx/effects";
import { AccountActionTypes } from "../account.actions";
import { RouterExtensions } from "nativescript-angular/router";
import { tap } from "rxjs/operators";

@Injectable()
export class TNSLoginEffects {
  @Effect({ dispatch: false })
  loginSuccess$ = this.actions$.pipe(
    ofType(AccountActionTypes.LOGIN_SUCCESS),
    tap(() => {
      this.router.navigate(["/list"], { clearHistory: true });
    })
  );

  constructor(private actions$: Actions, private router: RouterExtensions) {}
}
