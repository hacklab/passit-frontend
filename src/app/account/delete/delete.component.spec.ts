import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { RouterTestingModule } from "@angular/router/testing";
import { NgrxFormsModule } from "ngrx-forms";
import * as fromAccount from "../account.reducer";
import * as fromRoot from "../../app.reducers";
import { StoreModule } from "@ngrx/store";
import { InlineSVGModule } from "ng-inline-svg";
import { HttpClientModule } from "@angular/common/http";
import { DeleteContainer } from ".";
import { UserService } from "../user";

import { DeleteComponent } from "./delete.component";

describe("DeleteComponent", () => {
  let component: DeleteContainer;
  let fixture: ComponentFixture<DeleteContainer>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        NgrxFormsModule,
        InlineSVGModule.forRoot(),
        RouterTestingModule,
        HttpClientModule,
        StoreModule.forRoot(fromRoot.reducers),
        StoreModule.forFeature("account", fromAccount.reducers)
      ],
      declarations: [DeleteComponent, DeleteContainer],
      providers: [
        {
          provide: UserService
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteContainer);
    component = fixture.componentInstance;
  });

  it("should be created", () => {
    expect(component).toBeTruthy();
  });
});
