import {
  tap,
  withLatestFrom,
  map,
  exhaustMap,
  catchError
} from "rxjs/operators";
import { of } from "rxjs";

import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Actions, Effect, ofType } from "@ngrx/effects";

import {
  AccountActionTypes,
  LoginAction,
  LoginFailureAction,
  LoginSuccessAction,
  LogoutSuccessAction
} from "./account.actions";
import * as fromAccount from "./account.reducer";

import { IS_EXTENSION } from "../constants";
import { UserService } from "./user";
import { Store, select } from "@ngrx/store";
import { IState } from "../app.reducers";
import { SetPasswordActionTypes } from "./reset-password/set-password/set-password.actions";

@Injectable()
export class LoginEffects {
  @Effect()
  login$ = this.actions$.pipe(
    ofType<LoginAction>(AccountActionTypes.LOGIN),
    withLatestFrom(this.store.pipe(select(fromAccount.getLoginForm))),
    map(([action, form]) => form.value),
    exhaustMap(auth => {
      const callLogin = () => {
        return this.userService
          .login(
            auth.email,
            auth.password,
            auth.rememberMe ? auth.rememberMe : false
          )
          .pipe(
            map(resp => new LoginSuccessAction(resp)),
            catchError(err => of(new LoginFailureAction(err)))
          );
      };
      const callCheckAndSetUrl = (url: string) => {
        return this.userService.checkAndSetUrl(url).pipe(
          exhaustMap(() => callLogin()),
          catchError(err => of(new LoginFailureAction(err)))
        );
      };

      if (auth.url) {
        return callCheckAndSetUrl(auth.url);
      } else {
        return callLogin();
      }
    })
  );

  @Effect({ dispatch: false })
  loginSuccess$ = this.actions$.pipe(
    ofType(
      AccountActionTypes.LOGIN_SUCCESS,
      SetPasswordActionTypes.SET_PASSWORD_SUCCESS
    ),
    tap(() => {
      if (IS_EXTENSION) {
        this.router.navigate(["/popup"]);
      } else {
        this.router.navigate(["/list"]);
      }
    })
  );

  @Effect({ dispatch: false })
  loginRedirect$ = this.actions$.pipe(
    ofType(
      AccountActionTypes.LOGIN_REDIRECT,
      AccountActionTypes.LOGOUT_SUCCESS
    ),
    tap(() => {
      this.router.navigate(["/login"], { replaceUrl: true });
    })
  );

  @Effect()
  logout$ = this.actions$.pipe(
    ofType(AccountActionTypes.LOGOUT),
    exhaustMap(() =>
      this.userService
        .logout()
        .then(() => new LogoutSuccessAction())
        .catch(() => new LogoutSuccessAction())
    )
  );

  @Effect({ dispatch: false })
  logoutSuccess$ = this.actions$.pipe(
    ofType(AccountActionTypes.LOGOUT_SUCCESS),
    tap(() => localStorage.clear()),
    tap(() => this.router.navigate(["/login"]))
  );

  @Effect({ dispatch: false })
  userMustConfirmEmail$ = this.actions$.pipe(
    ofType(AccountActionTypes.USER_MUST_CONFIRM_EMAIL),
    tap(() => this.router.navigate(["/confirm-email"]))
  );

  constructor(
    private actions$: Actions,
    private userService: UserService,
    private router: Router,
    private store: Store<IState>
  ) {}
}
