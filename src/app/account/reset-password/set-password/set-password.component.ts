import {
  Component,
  ChangeDetectionStrategy,
  Input,
  Output,
  EventEmitter
} from "@angular/core";
import { FormGroupState } from "ngrx-forms";
import { ISetPasswordForm } from "./set-password.reducer";

@Component({
  selector: "app-set-password",
  templateUrl: "./set-password.component.html",
  styleUrls: ["../../account.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetPasswordComponent {
  @Input()
  form: FormGroupState<ISetPasswordForm>;
  @Input()
  hasStarted: boolean;
  @Output()
  setPassword = new EventEmitter();
  @Output()
  toggleConfirm = new EventEmitter();

  onSubmit() {
    if (this.form.isValid) {
      this.setPassword.emit();
    }
  }
}
