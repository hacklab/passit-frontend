import {
  validate,
  updateGroup,
  createFormGroupState,
  FormGroupState,
  createFormStateReducerWithUpdate
} from "ngrx-forms";
import { minLength } from "ngrx-forms/validation";
import { IBaseFormState } from "../../../utils/interfaces";
import {
  ResetPasswordVerifyActionTypes,
  ResetPasswordVerifyActionsUnion
} from "./reset-password-verify.actions";

const FORM_ID = "Reset Password Form";

export interface IResetPasswordVerifyForm {
  code: string;
}

const validateAndUpdateResetPasswordVerifyFormState = updateGroup<
  IResetPasswordVerifyForm
>({
  code: validate(minLength(32))
});

export const initialResetPasswordVerifyFormState = validateAndUpdateResetPasswordVerifyFormState(
  createFormGroupState<IResetPasswordVerifyForm>(FORM_ID, {
    code: ""
  })
);

export interface IResetPasswordVerifyState extends IBaseFormState {
  form: FormGroupState<IResetPasswordVerifyForm>;
  errorMessage: string | null;
  email: string | null;
  emailCode: string | null;
}

const initialState: IResetPasswordVerifyState = {
  form: initialResetPasswordVerifyFormState,
  hasStarted: false,
  hasFinished: false,
  errorMessage: null,
  email: null,
  emailCode: null
};

export const formReducer = createFormStateReducerWithUpdate<
  IResetPasswordVerifyForm
>(validateAndUpdateResetPasswordVerifyFormState);

export function reducer(
  state = initialState,
  action: ResetPasswordVerifyActionsUnion
): IResetPasswordVerifyState {
  const form = formReducer(state.form, action);
  state = { ...state, form };

  switch (action.type) {
    case ResetPasswordVerifyActionTypes.INIT:
      if (action.payload.email && action.payload.code) {
        return {
          ...state,
          email: action.payload.email,
          emailCode: action.payload.code,
          errorMessage: null
        };
      } else {
        return {
          ...state,
          errorMessage: "Invalid reset password link"
        };
      }

    case ResetPasswordVerifyActionTypes.VERIFY_AND_LOGIN:
      return {
        ...state,
        hasStarted: true
      };

    case ResetPasswordVerifyActionTypes.VERIFY_AND_LOGIN_FAILURE:
      return {
        ...state,
        hasStarted: false,
        errorMessage: "Unable to validate backup code. Verify that this is your most recent code and that it was entered correctly."
      };
  }

  return state;
}

export const getForm = (state: IResetPasswordVerifyState) => state.form;
export const getHasStarted = (state: IResetPasswordVerifyState) =>
  state.hasStarted;
export const getErrorMessage = (state: IResetPasswordVerifyState) =>
  state.errorMessage;
export const getEmailAndCode = (state: IResetPasswordVerifyState) => {
  return { email: state.email, emailCode: state.emailCode };
};
