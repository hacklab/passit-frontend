import { Component, OnInit } from "@angular/core";
import * as fromAccount from "../../account.reducer";
import { Store, select } from "@ngrx/store";
import { Init, VerifyAndLogin } from "./reset-password-verify.actions";
import { ActivatedRoute } from "@angular/router";
import { BACKUP_CODE_CHARS, BACKUP_CODE_LENGTH } from "../constants";

@Component({
  template: `
    <app-reset-password-verify
      [form]="form$ | async"
      [hasStarted]="hasStarted$ | async"
      [errorMessage]="errorMessage$ | async"
      [badCodeError]="badCodeError"
      (verify)="verify($event)"
    ></app-reset-password-verify>
  `
})
export class ResetPasswordVerifyContainer implements OnInit {
  form$ = this.store.pipe(select(fromAccount.getResetPasswordVerifyForm));
  hasStarted$ = this.store.pipe(
    select(fromAccount.getResetPasswordVerifyHasStarted)
  );
  errorMessage$ = this.store.pipe(
    select(fromAccount.getResetPasswordVerifyErrorMessage)
  );
  badCodeError: string;

  constructor(
    private store: Store<fromAccount.IAuthState>,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    const payload = {
      code: this.route.snapshot.queryParamMap.get("code"),
      email: this.route.snapshot.queryParamMap.get("email")
    };
    this.store.dispatch(new Init(payload));
  }

  verify(backupCode: string) {
    backupCode = backupCode.replace(/\s/g, "").toUpperCase();
    if (
      backupCode.length === BACKUP_CODE_LENGTH &&
      !backupCode.match(new RegExp(`[^${BACKUP_CODE_CHARS}\-]`))
    ) {
      this.store.dispatch(new VerifyAndLogin(backupCode));
    } else {
      // This error is for coedes that don't match the 32 letter and spacing requirements.
      // If it is a Passit code, but the wrong one, it will error in in effects.
      this.badCodeError = "Passit does not recognize this backup code.";
    }
  }
}
