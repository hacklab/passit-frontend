import { Action } from "@ngrx/store";
import { IAuthStore } from "../../user";

export enum ResetPasswordVerifyActionTypes {
  INIT = "[Reset Password Verify] Init",
  VERIFY_AND_LOGIN = "[Reset Password Verify] Verify and Login",
  VERIFY_AND_LOGIN_SUCCESS = "[Reset Password Verify] Verify and Login Success",
  VERIFY_AND_LOGIN_FAILURE = "[Reset Password Verify] Verify and Login Failure",
  SET_ERROR = "[Reset Password Verify] Set error"
}

export class Init implements Action {
  readonly type = ResetPasswordVerifyActionTypes.INIT;

  constructor(public payload: { email: string | null; code: string | null }) {}
}

export class VerifyAndLogin implements Action {
  readonly type = ResetPasswordVerifyActionTypes.VERIFY_AND_LOGIN;

  constructor(public payload: string) {}
}

export class VerifyAndLoginSuccess implements Action {
  readonly type = ResetPasswordVerifyActionTypes.VERIFY_AND_LOGIN_SUCCESS;

  constructor(public payload: IAuthStore) {}
}

export class VerifyAndLoginFailure implements Action {
  readonly type = ResetPasswordVerifyActionTypes.VERIFY_AND_LOGIN_FAILURE;

  constructor(public payload: any) {}
}

export class SetError implements Action {
  readonly type = ResetPasswordVerifyActionTypes.SET_ERROR;

  constructor(public payload: string) {}
}

export type ResetPasswordVerifyActionsUnion =
  | Init
  | VerifyAndLogin
  | VerifyAndLoginSuccess
  | VerifyAndLoginFailure;
