import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  ViewChild
} from "@angular/core";
import { AbstractControlState } from "ngrx-forms";

@Component({
  selector: "app-text-field",
  template: `<StackLayout class="input-field m-b-10">
               <Label class="text-label" [text]="label"></Label>
               <TextField
                   #textFieldInput
                   class="input text-field"
                   [class.text-field--valid]="ngrxFormControl.isValid && extraValidations && !overrideValidStyling"
                   [class.text-field--error]="(isFormSubmitted || showErrorBeforeSubmit) && (!ngrxFormControl.isValid || !extraValidations)"
                   [autocapitalizationType]="autocapitalizationType"
                   [autocorrect]="autocorrect"
                   [editable]="editable"
                   [hint]="hint"
                   [keyboardType]="keyboardType"
                   [ngrxFormControlState]="ngrxFormControl"
                   [returnKeyType]="returnKeyType"
                   [secure]="secure"
                   [attr.width]="width ? width : null"
                   (focus)="focus.emit()"
                   (returnPress)="returnPress.emit()"></TextField>
             </StackLayout>`,
  styles: [
    `
      .text-label {
        color: #413741;
        font-size: 14;
        letter-spacing: 0.04;
        line-height: 1.333;
      }
      .text-field {
        padding: 10 0;
        border-bottom-width: 2;
        border-bottom-color: #6f989e;
      }
      .text-field::highlighted {
        border-bottom-color: #0092a8;
      }
      .text-field--error::highlighted {
        border-bottom-color: #b92855;
      }
      .text-field--valid::highlighted {
        border-bottom-color: #6cc780;
      }
    `
  ],
  moduleId: module.id,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TextFieldComponent {
  @Input()
  autocapitalizationType: string;
  @Input()
  autocorrect: boolean;
  @Input()
  editable = true;
  @Input()
  isFormSubmitted: boolean;
  @Input()
  hint = "";
  @Input()
  keyboardType: string;
  @Input()
  label: string;
  @Input()
  ngrxFormControl: AbstractControlState<string>;
  @Input()
  returnKeyType: string;
  @Input()
  secure = false;
  @Input()
  width: number;
  @Input()
  extraValidations = true;
  @Input()
  overrideValidStyling = false;
  @Input()
  showErrorBeforeSubmit = false;
  @Output()
  returnPress = new EventEmitter();
  @Output()
  focus = new EventEmitter();
  @ViewChild("textFieldInput")
  textFieldInput: ElementRef;

  constructor() {}

  focusInput = () => {
    this.textFieldInput.nativeElement.focus();
  };
}
