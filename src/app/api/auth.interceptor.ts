import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse
} from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";
import { Store, select } from "@ngrx/store";
import { getToken } from "~/app/account/account.reducer";
import {
  LogoutSuccessAction,
  UserMustConfirmEmailAction
} from "../account/account.actions";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  token: string | null;

  constructor(private store: Store<any>) {
    this.store.pipe(select(getToken)).subscribe(token => (this.token = token));
  }

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (this.token) {
      request = request.clone({
        setHeaders: {
          Authorization: `token ${this.token}`
        }
      });
    }

    return next.handle(request).pipe(
      tap(
        (event: HttpEvent<any>) => {},
        (err: any) => {
          if (err instanceof HttpErrorResponse && err.status === 403) {
            const detail = err.error.detail;
            if (
              detail === "Authentication credentials were not provided." ||
              detail === "Invalid token."
            ) {
              this.store.dispatch(new LogoutSuccessAction());
            } else if (detail === "User's email is not confirmed.") {
              this.store.dispatch(new UserMustConfirmEmailAction());
            }
          }
        }
      )
    );
  }
}
