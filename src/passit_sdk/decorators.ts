/* tslint:disable:only-arrow-functions */
/* tslint:disable:object-literal-shorthand */
import { AuthenticationRequiredException } from "./exceptions";

export function authRequired(target: any, key: any, descriptor: any) {
  return {
    descriptor,
    value: function() {
      if (!this._check_if_authenticated()) {
        return Promise.reject(new AuthenticationRequiredException(key));
      }
      return descriptor.value.apply(this, arguments);
    }
  };
}
